#include "account_handler.hpp"
#include "string"
#include "utils.hpp"
#include <vector>
#include <fstream>
#include <iostream>

std::map<std::string, int> rusMonth {
    { "январь", 1 },
    { "февраль", 2 },
    { "март", 3 },
    { "апрель", 4 },
    { "май", 5 },
    { "июнь", 6 },
    { "июль", 7 },
    { "август", 8 },
    { "сентябрь", 9 },
    { "октябрь", 10 },
    { "ноябрь", 11 },
    { "декабрь", 12 }
};

AccountHandler::AccountHandler(string schema) {
    ifstream i(schema);
    try {    
        i >> schema_;
    } catch(json::exception& e) {
        std::cout << "message: " << e.what() << '\n'
                  << "exception id: " << e.id << std::endl;
    }
}

void AccountHandler::handle(string & line) {
    // start of account *___*
    if ( *line.begin() == '*' && *(--line.end()) == '*' ) {
        lineInAccount_ = 0;
    }

    // waiting for start
    if ( lineInAccount_ < 0 ) {
        return;
    }

    // end of account ----
    if ( lineInAccount_ > 7 && *(line.begin()+2) == '-' && *(line.end()-3) == '-' ) {
        lineInAccount_ = 0;
        parse();
        account_.clear();
    }

    if ( 0 != lineInAccount_) {
        account_.push_back(line);    
    }

    lineInAccount_++;
};

void AccountHandler::parse() {
    int numOfLines = account_.size();
    
    for (auto line = account_.begin(); line != account_.end(); ++line) {
        int currentLine = distance(account_.begin(), line); 
        line_ = *line;
        
        if ( currentLine < 3 ) {
            setEntries(currentLine);
        } else if ( 6 < currentLine && currentLine < ( numOfLines - 2 ) ) {
            string month = parseEntry(3, "month");
            if ( month.empty() ) { return; }
            if ( stoi(month) == rusMonth[cache_["accountMonth"].get<string>()] ) {
                setGroupEntries(3, "daysByType", "typeOfPayment", "days");
                setGroupEntries(3, "sumByType", "typeOfPayment", "sum");
            }
        } else if ( currentLine == numOfLines - 2 ) {
            setEntries(4);
        } else {
            continue;
        }

        line_.clear();
    }

    dump();
    cache_.clear();
};

string AccountHandler::parseEntry(int schemaKey, string name) {
    string entry;
    auto ar = schema_["entries"][schemaKey][name].get<std::vector<int>>();
    trim(entry = mbSubstr(line_, ar[0], ar[1]));
    return entry;
}

void AccountHandler::dump() {
    std::ofstream outputCache("cache.json");
    outputCache << std::setw(4) << cache_ << std::endl;

    std::ofstream outputAcccount("results/" + cache_["tabNum"].get<string>() + ".json");
    outputAcccount << std::setw(4) << cache_ << std::endl;
}

void AccountHandler::setEntries(int schemaKey) {
    for (auto& el: schema_["entries"][schemaKey].items() ) {
        auto ar = schema_["entries"][schemaKey][el.key()].get<std::vector<int>>();
        string result = mbSubstr(line_, ar[0], ar[1]);
        trim(result);
        cache_[el.key()] = result;
    }
}

void AccountHandler::setGroupEntries(int schemaKey, string group, string typeName, string valueName) {
    string tn = parseEntry(schemaKey, typeName);
    string vn = parseEntry(schemaKey, valueName);
    
    if ( !tn.empty() && !vn.empty() ) {
        if ( cache_[group].empty() ) {
            cache_[group] = json::object();
        }

        if ( cache_[group][tn].empty() ) {
            cache_[group][tn] = 0.0;
        }

        cache_[group][tn] = cache_[group][tn].get<double>() + stod(vn);
    }
}