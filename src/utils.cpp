#include "utils.hpp"

// trim from start (in place)
void ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
        return !std::isspace(ch);
    }));
}

// trim from end (in place)
void rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
        return !std::isspace(ch);
    }).base(), s.end());
}

// trim from both ends (in place)
void trim(std::string &s) {
    ltrim(s);
    rtrim(s);
}

std::string mbSubstr(const std::string str, int& start, int& length) {
    int l = str.length();
    unsigned char c;
    unsigned int bytes = 0;
    int mb_start, mb_length;
    int char_num = 0;
    for (int bytes = 0; bytes <= l; bytes++) {
        if ( char_num == start ) {
            mb_start = bytes;
        }

        if ( char_num == ( start + length ) ) {
            mb_length = bytes - mb_start;
            break;
        }

        c = (unsigned char) str[bytes];
        // TODO понять логику
        if (c<=127) bytes+=0;
        else if ((c & 0xE0) == 0xC0) bytes+=1;
        else if ((c & 0xF0) == 0xE0) bytes+=2;
        else if ((c & 0xF8) == 0xF0) bytes+=3;
        else if ((c & 0xFC) == 0xF8) bytes+=4; // 111110bb //byte 5, unnecessary in 4 byte UTF-8
        else if ((c & 0xFE) == 0xFC) bytes+=5; // 1111110b //byte 6, unnecessary in 4 byte UTF-8
        else return "";

        char_num++;
    }

    return str.substr(mb_start, mb_length);
}