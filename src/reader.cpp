#include "duckx.hpp"
#include <string>
#include "reader.hpp"

using namespace std;

void Reader::read(IHandler & handler) {
    document_.open();
    string line; 
    int i = 0; 
    for (auto p : document_.paragraphs()) {
        // collect parts to line
        for (auto r : p.runs()) {
            line += r.get_text(); 
        }
        
        handler.handle(line);
        
        line.clear();
    }
};
