#ifndef AM_ACCOUNT_HANDLER_HPP
#define AM_ACCOUNT_HANDLER_HPP

#include "handler.hpp"
#include <string>
#include <vector>
#include <nlohmann/json.hpp>

using namespace std;
using json = nlohmann::json;

class AccountHandler : public IHandler {
    private:
        int lineInAccount_ = -1;
        vector<string> account_;
        json cache_;
        json schema_;
        string line_;
    public:
        AccountHandler(string schema);
        void handle(string & line);
        void parse();
        string parseEntry(int schemaKey, string name);
        void setEntries(int schemaKey);
        void setGroupEntries(int schemaKey, string group, string typeName, string valueName);
        void dump();
};

#endif