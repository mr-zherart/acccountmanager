#ifndef AM_UTILS_HPP
#define AM_UTILS_HPP

#include <string>
#include <map>

// trim from start (in place)
void ltrim(std::string &s);
// trim from end (in place)
void rtrim(std::string &s);
// trim from both ends (in place)
void trim(std::string &s);
std::string mbSubstr(const std::string str, int& start, int& length);

#endif