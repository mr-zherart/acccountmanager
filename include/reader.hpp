#ifndef AM_READER_HPP
#define AM_READER_HPP

#include "duckx.hpp"
#include "handler.hpp"

class Reader
{
    public:
        Reader(std::string filepath) {
            // TODO strategy of usage
            document_ = duckx::Document(filepath); 
        }
        void read(IHandler & handler);
    private:
        duckx::Document document_;
};

#endif