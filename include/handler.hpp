#ifndef AM_HANDLER_HPP
#define AM_HANDLER_HPP

#include <string>

class IHandler {
    public:
        IHandler() {}
        virtual ~IHandler() {}
        virtual void handle(std::string & line) = 0;
        virtual void parse() = 0; 
};

#endif