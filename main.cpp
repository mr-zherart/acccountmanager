#include "reader.hpp"
#include "account_handler.hpp"

int main() {
    AccountHandler handler = AccountHandler("./schema.json");

    Reader reader = Reader("./test.docx");
    reader.read(handler);
}