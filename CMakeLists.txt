cmake_minimum_required(VERSION 3.1)

set(CMAKE_C_COMPILER "/usr/bin/gcc")
set(CMAKE_CXX_COMPILER "/usr/bin/g++")
set(CMAKE_CXX_FLAGS "-std=c++11 -stdlib=libc++")
set(JSON_BuildTests OFF CACHE INTERNAL "")

project(AccountManager VERSION 0.1.0 LANGUAGES C CXX)


include_directories(include lib/Duckx/include lib/Duckx/thirdparty/pugixml lib/Duckx/thirdparty/zip)
add_subdirectory(lib/duckx)
add_subdirectory(lib/nlohmann_json)

file(GLOB SOURCES
     "${CMAKE_SOURCE_DIR}/src/*.cpp"
)

include(CTest)
enable_testing()

add_executable(AccountManager main.cpp ${SOURCES})
target_link_libraries(AccountManager PRIVATE nlohmann_json::nlohmann_json duckx::duckx)

set(CPACK_PROJECT_NAME ${PROJECT_NAME})
set(CPACK_PROJECT_VERSION ${PROJECT_VERSION})
include(CPack)
